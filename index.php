<?php

declare(strict_types=1);

// Delegate static file requests back to the PHP built-in webserver
if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) {
    return false;
}

chdir(dirname(__DIR__));
require 'vendor/autoload.php';

// Hide the global scope
echo (function () {
    $response         = null;
    $configAggregator = new \Smtm\Frameless\Config\ConfigAggregator();
    $config           = $configAggregator->aggregate(__DIR__ . '/config/config.php');
    $containerFactory = new \Smtm\Psr\Container\Factory\ContainerFactory();
    $container        = $containerFactory->create($config);

    $response = null;
    try {
        $router         = $container->get(\Smtm\Frameless\Router\Router::class);
        $currentRequest = $container->get(\Smtm\Psr\Http\Message\Request\CurrentRequestDecorator::class);
        $stack          = $router->match($currentRequest);
        $lastRoute      = end($stack);
        reset($stack);
        if (is_callable($lastRoute->getAction())) {
            $response = $lastRoute->getAction()();
        } else {
            $controller = $container->get($lastRoute->getAction()['controller']);
            $response   = $controller->{$lastRoute->getAction()['method']}();
        }
    } catch (\Throwable $e) {
        $response = $e;
    }
    $view = $container->get(\Smtm\Mvc\View\ViewTemplateDecorator::class);
    //return $view->renderModel($response);
    return $response;
})();
